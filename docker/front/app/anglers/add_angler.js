import {clearElementChildren, createLinkCell, createButtonCell, createTextCell} from '../js/dom_utils.js';
import {getBackendUrl} from '../js/configuration.js';

window.addEventListener('load', () => {
    setupSubmit()
});

function setupSubmit() {
    const form = document.getElementById('form');
    form.addEventListener('submit', onSubmit);
}

function onSubmit() {

    event.preventDefault();

    const form = document.getElementById('form');
    var obj = {};
    var formData = new FormData(form);
    for (var key of formData.keys()) {
        obj[key] = formData.get(key);
    }

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 201) {
            location.href = 'anglers_list.html'
        }
    };
    xhttp.open("POST", getBackendUrl() + '/api/anglers', true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(obj));


}