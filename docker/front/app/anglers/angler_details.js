import {clearElementChildren, createLinkCell, createButtonCell, createTextCell} from '../js/dom_utils.js';
import {getBackendUrl} from '../js/configuration.js';

window.addEventListener('load', () => {
    fetchAndDisplayAnglerData();
    fetchAndDisplayFishes();
});

function fetchAndDisplayAnglerData() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            displayAnglerData(JSON.parse(this.responseText))
            var a = document.getElementById('add_fish')
            a.href = "../fish/add_fish.html?angler=" + getParameterByName("angler")
        }
    };
    xhttp.open("GET", getBackendUrl() + '/api/anglers/' + getParameterByName("angler"), true);
    xhttp.send();
}

function fetchAndDisplayFishes() {
    const xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            displayFishData(JSON.parse(this.responseText))
        }
    };
    xhttp2.open("GET", getBackendUrl() + '/api/anglers/' + getParameterByName("angler") + "/fishes", true);
    xhttp2.send();
}

function displayAnglerData(angler) {
    let tableBody = document.getElementById('tableBody');
    clearElementChildren(tableBody);
    tableBody.appendChild(createTableRow(angler));
}

function displayFishData(fishes) {
    let tableBody = document.getElementById('fishTable');
    clearElementChildren(tableBody);
    fishes.fishes.forEach(fish => {
        tableBody.appendChild(createFishTableRow(fish["species"], fish["id"]));
    })
}

function createTableRow(angler) {
    let tr = document.createElement('tr');
    tr.appendChild(createTextCell(angler["id"]));
    tr.appendChild(createTextCell(angler["firstName"]));
    tr.appendChild(createTextCell(angler["lastName"]));
    tr.appendChild(createTextCell(angler["experience"]));
    return tr;
}

function createFishTableRow(species, id) {
    let tr = document.createElement('tr');
    tr.appendChild(createTextCell(id));
    tr.appendChild(createTextCell(species));
    tr.appendChild(createLinkCell('view', '../fish/fish_details.html?fish=' + id + "&angler=" + getParameterByName("angler")));
    tr.appendChild(createLinkCell('edit', '../fish/edit_fish.html?fish=' + id + "&angler=" + getParameterByName("angler")));
    tr.appendChild(createButtonCell('delete', () => deleteFish(id)));
    return tr;
}

function deleteFish(id) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 202) {
            fetchAndDisplayFishes();
        }
    };
    xhttp.open("DELETE", getBackendUrl() + '/api/fishes/' + id, true);
    xhttp.send();
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}