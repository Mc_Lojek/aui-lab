import {clearElementChildren, createLinkCell, createButtonCell, createTextCell} from '../js/dom_utils.js';
import {getBackendUrl} from '../js/configuration.js';

window.addEventListener('load', () => {
    setupSubmit()
    fetchAngler(getParameterByName("angler"))
});

function fillForm(angler) {

    document.getElementById('first').value = angler["firstName"]
    document.getElementById('last').value = angler["lastName"]
    document.getElementById('exp').value = angler["experience"]
}

function setupSubmit() {
    const form = document.getElementById('form');
    form.addEventListener('submit', onSubmit);
}

function onSubmit() {

    event.preventDefault();

    const form = document.getElementById('form');
    var obj = {};
    var formData = new FormData(form);
    for (var key of formData.keys()) {
        obj[key] = formData.get(key);
    }

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 202) {
            location.href = 'anglers_list.html'
        }
    };
    xhttp.open("PUT", getBackendUrl() + '/api/anglers/' + getParameterByName("angler"), true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(obj));
}

function fetchAngler(id) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            fillForm(JSON.parse(this.responseText))
        }
    };
    xhttp.open("GET", getBackendUrl() + '/api/anglers/' + id, true);
    xhttp.send();
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}