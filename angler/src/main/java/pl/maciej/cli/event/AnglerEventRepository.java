package pl.maciej.cli.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import pl.maciej.cli.model.Angler;

@Repository
public class AnglerEventRepository {

    private RestTemplate restTemplate;

    @Autowired
    public AnglerEventRepository(@Value("${rpg.characters.url}") String baseUrl) {
        restTemplate = new RestTemplateBuilder().rootUri(baseUrl).build();
    }

    public void delete(Angler angler) {
        restTemplate.delete("/anglers/{id}", angler.getId());
    }

    public void create(Angler angler) {
        restTemplate.postForLocation("/anglers/", CreateAnglerRequest.entityToDtoMapper().apply(angler));
    }

}
