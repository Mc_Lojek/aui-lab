package pl.maciej.cli.event;

import lombok.*;
import pl.maciej.cli.model.Angler;

import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class CreateAnglerRequest {

    private int id;

    public static Function<Angler, CreateAnglerRequest> entityToDtoMapper() {
        return entity -> CreateAnglerRequest.builder()
                .id(entity.getId())
                .build();
    }
}
