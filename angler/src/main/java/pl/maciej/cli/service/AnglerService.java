package pl.maciej.cli.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.maciej.cli.event.AnglerEventRepository;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.repository.AnglerRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AnglerService {

    private final AnglerRepository repository;
    private final AnglerEventRepository eventRepository;

    public Optional<Angler> find(int id) {
        return repository.findById(id);
    }

    public List<Angler> findAll() {
        return repository.findAll();
    }

    @Transactional
    public void create(final Angler a) {
        repository.save(a);
        eventRepository.create(a);
    }

    @Transactional
    public void update(Angler a) {
        repository.save(a);
    }

    @Transactional
    public void delete(Angler angler) {
        repository.delete(angler);
        eventRepository.delete(angler);
    }

}
