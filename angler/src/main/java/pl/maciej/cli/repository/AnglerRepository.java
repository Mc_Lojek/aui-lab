package pl.maciej.cli.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.maciej.cli.model.Angler;

@Repository
public interface AnglerRepository extends JpaRepository<Angler, Integer> {

}