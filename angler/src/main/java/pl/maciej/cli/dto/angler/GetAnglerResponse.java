package pl.maciej.cli.dto.angler;

import lombok.*;
import pl.maciej.cli.model.Angler;

import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class GetAnglerResponse {

    private int id;
    private String firstName;
    private String lastName;
    private int experience;

    public static Function<Angler, GetAnglerResponse> entityToDtoMapper() {
        return angler -> GetAnglerResponse.builder()
                .id(angler.getId())
                .firstName(angler.getFirstName())
                .lastName(angler.getLastName())
                .experience(angler.getExperience())
                .build();
    }

}
