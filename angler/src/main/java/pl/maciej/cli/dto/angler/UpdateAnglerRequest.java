package pl.maciej.cli.dto.angler;

import lombok.*;
import pl.maciej.cli.model.Angler;

import java.util.function.BiFunction;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class UpdateAnglerRequest {

    private String firstName;
    private String lastName;
    private int experience;

    public static BiFunction<Angler, UpdateAnglerRequest, Angler> dtoToEntityUpdater() {
        return (angler, request) -> {
            angler.setFirstName(request.getFirstName());
            angler.setLastName(request.getLastName());
            angler.setExperience(request.getExperience());
            return angler;
        };
    }

}
