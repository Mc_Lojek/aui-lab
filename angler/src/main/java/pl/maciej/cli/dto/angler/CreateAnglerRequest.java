package pl.maciej.cli.dto.angler;

import lombok.*;
import pl.maciej.cli.model.Angler;

import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class CreateAnglerRequest {

    private String firstName;
    private String lastName;
    private int experience;

    public static Function<CreateAnglerRequest, Angler> dtoToEntityMapper() {
        return request -> Angler.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .experience(request.getExperience())
                .build();
    }

}
