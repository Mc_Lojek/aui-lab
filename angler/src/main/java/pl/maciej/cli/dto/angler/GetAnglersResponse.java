package pl.maciej.cli.dto.angler;

import lombok.*;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class GetAnglersResponse {

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @ToString
    @EqualsAndHashCode
    public static class Angler {
        private int id;
        private String firstName;
    }

    @Singular
    private List<Angler> anglers;

    public static Function<Collection<pl.maciej.cli.model.Angler>, GetAnglersResponse> entityToDtoMapper() {
        return anglers -> {
            GetAnglersResponse.GetAnglersResponseBuilder response = GetAnglersResponse.builder();
            anglers.stream()
                    .map(angler -> Angler.builder()
                            .id(angler.getId())
                            .firstName(angler.getFirstName())
                            .build())
                    .forEach(response::angler);
            return response.build();
        };
    }

}
