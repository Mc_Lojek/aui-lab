package pl.maciej.cli.utils;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.service.AnglerService;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class Initializer {

    private final AnglerService anglerService;

    @PostConstruct
    public synchronized void init() {

        if (anglerService.find(0).isEmpty()) {
            Angler a1 = Angler.builder()
                    .firstName("Joe")
                    .lastName("Smith")
                    .experience(4)
                    .build();

            Angler a2 = Angler.builder()
                    .firstName("Mikalai")
                    .lastName("Neon")
                    .experience(7)
                    .build();

            Angler a3 = Angler.builder()
                    .firstName("Steve")
                    .lastName("Brooks")
                    .experience(2)
                    .build();

            anglerService.create(a1);
            anglerService.create(a2);
            anglerService.create(a3);
        }
    }

}
