package pl.maciej.cli.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.maciej.cli.dto.angler.CreateAnglerRequest;
import pl.maciej.cli.dto.angler.GetAnglerResponse;
import pl.maciej.cli.dto.angler.GetAnglersResponse;
import pl.maciej.cli.dto.angler.UpdateAnglerRequest;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.service.AnglerService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
@RestController
@RequestMapping("api/anglers")
public class AnglerController {

    private final AnglerService anglerService;

    @GetMapping
    public ResponseEntity<GetAnglersResponse> getAnglers() {
        List<Angler> all = anglerService.findAll();
        Function<Collection<Angler>, GetAnglersResponse> mapper = GetAnglersResponse.entityToDtoMapper();
        GetAnglersResponse response = mapper.apply(all);
        return ResponseEntity.ok(response);
    }

    @GetMapping("{id}")
    public ResponseEntity<GetAnglerResponse> getAngler(@PathVariable("id") int id) {
        return anglerService.find(id)
                .map(value -> ResponseEntity.ok(GetAnglerResponse.entityToDtoMapper().apply(value)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Void> createAngler(@RequestBody CreateAnglerRequest request, UriComponentsBuilder builder) {
        Angler angler = CreateAnglerRequest
                .dtoToEntityMapper()
                .apply(request);
        anglerService.create(angler);
        return ResponseEntity.created(builder.pathSegment("api", "characters", "{id}")
                .buildAndExpand(angler.getId()).toUri()).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteAngler(@PathVariable("id") int id) {
        Optional<Angler> angler = anglerService.find(id);
        if (angler.isPresent()) {
            anglerService.delete(angler.get());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> updateAngler(@RequestBody UpdateAnglerRequest request, @PathVariable("id") int id) {
        Optional<Angler> angler = anglerService.find(id);
        if (angler.isPresent()) {
            UpdateAnglerRequest.dtoToEntityUpdater().apply(angler.get(), request);
            anglerService.update(angler.get());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
