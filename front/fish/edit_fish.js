import {clearElementChildren, createLinkCell, createButtonCell, createTextCell} from '../js/dom_utils.js';
import {getBackendUrl} from '../js/configuration.js';

window.addEventListener('load', () => {
    setupSubmit()
    fetchFish(getParameterByName("fish"))
});

function fillForm(fish) {

    document.getElementById('species').value = fish["species"]
    document.getElementById('weight').value = fish["weight"]
}

function setupSubmit() {
    const form = document.getElementById('form');
    form.addEventListener('submit', onSubmit);
}

function onSubmit() {

    event.preventDefault();

    const form = document.getElementById('form');
    var obj = {};
    var formData = new FormData(form);
    for (var key of formData.keys()) {
        obj[key] = formData.get(key);
    }
    obj["angler"] = getParameterByName("angler")

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 202) {
            location.href = '../anglers/angler_details.html?angler=' + getParameterByName("angler");
        }
    };
    xhttp.open("PUT", getBackendUrl() + '/api/fishes/' + getParameterByName("fish"), true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(obj));


}

function fetchFish(id) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            fillForm(JSON.parse(this.responseText))
        }
    };
    xhttp.open("GET", getBackendUrl() + '/api/fishes/' + id, true);
    xhttp.send();
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}