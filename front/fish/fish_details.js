import {clearElementChildren, createLinkCell, createButtonCell, createTextCell} from '../js/dom_utils.js';
import {getBackendUrl} from '../js/configuration.js';

window.addEventListener('load', () => {
    fetchAndDisplayFishData();
});

function fetchAndDisplayFishData() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            displayFishData(JSON.parse(this.responseText))
        }
    };
    xhttp.open("GET", getBackendUrl() + '/api/anglers/' + getParameterByName("angler") + "/fishes/" + getParameterByName("fish"), true);
    xhttp.send();
}

function displayFishData(fish) {
    let tableBody = document.getElementById('tableBody');
    clearElementChildren(tableBody);
    tableBody.appendChild(createTableRow(fish));
}

function createTableRow(fish) {
    let tr = document.createElement('tr');
    tr.appendChild(createTextCell(fish["id"]));
    tr.appendChild(createTextCell(fish["species"]));
    tr.appendChild(createTextCell(fish["weight"]));
    tr.appendChild(createTextCell(fish["angler"]));
    return tr;
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}