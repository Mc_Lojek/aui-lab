import {clearElementChildren, createLinkCell, createButtonCell, createTextCell} from '../js/dom_utils.js';
import {getBackendUrl} from '../js/configuration.js';

window.addEventListener('load', () => {
    fetchAndDisplayAnglers();

});

function fetchAndDisplayAnglers() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            displayAnglers(JSON.parse(this.responseText))
        }
    };
    xhttp.open("GET", getBackendUrl() + '/api/anglers', true);
    xhttp.send();
}

function displayAnglers(anglers) {
    let tableBody = document.getElementById('tableBody');
    clearElementChildren(tableBody);
    anglers.anglers.forEach(angler => {
        tableBody.appendChild(createTableRow(angler["firstName"], angler["id"]));
    })
}

function createTableRow(angler, id) {
    let tr = document.createElement('tr');
    tr.appendChild(createTextCell(id));
    tr.appendChild(createTextCell(angler));
    tr.appendChild(createLinkCell('view', '../anglers/angler_details.html?angler=' + id));
    tr.appendChild(createLinkCell('edit', '../anglers/edit_angler.html?angler=' + id));
    tr.appendChild(createButtonCell('delete', () => deleteAngler(id)));
    return tr;
}

function deleteAngler(id) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 202) {
            fetchAndDisplayAnglers();
        }
    };
    xhttp.open("DELETE", getBackendUrl() + '/api/anglers/' + id, true);
    xhttp.send();
}