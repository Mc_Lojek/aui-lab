package pl.maciej.cli.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.repository.AnglerRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AnglerService {

    private final AnglerRepository repository;

    public Optional<Angler> find(int id) {
        return repository.findById(id);
    }

    @Transactional
    public Angler create(final Angler a) {
        return repository.save(a);
    }

    @Transactional
    public void delete(int id) {
        repository.deleteById(id);
    }

}
