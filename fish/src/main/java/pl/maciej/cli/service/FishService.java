package pl.maciej.cli.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.model.Fish;
import pl.maciej.cli.repository.AnglerRepository;
import pl.maciej.cli.repository.FishRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FishService {

    private final FishRepository repository;
    private final AnglerRepository anglerRepository;

    public Optional<Fish> find(int id) {
        return repository.findById(id);
    }

    public Optional<Fish> find(int anglerId, int fishId) {
        Optional<Angler> angler = anglerRepository.findById(anglerId);
        if (angler.isPresent()) {
            return repository.findByIdAndAngler(fishId, angler.get());
        } else {
            return Optional.empty();
        }
    }

    public List<Fish> findAll() {
        return repository.findAll();
    }

    public List<Fish> findAll(Angler angler) {
        return repository.findAllByAngler(angler);
    }

    @Transactional
    public Fish create(Fish f) {
        return repository.save(f);
    }

    @Transactional
    public void update(Fish f) {
        repository.save(f);
    }

    @Transactional
    public void delete(int f) {
        repository.deleteById(f);
    }

}