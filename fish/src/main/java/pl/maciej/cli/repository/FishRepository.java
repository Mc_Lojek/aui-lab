package pl.maciej.cli.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.model.Fish;

import java.util.List;
import java.util.Optional;

@Repository
public interface FishRepository extends JpaRepository<Fish, Integer> {

    List<Fish> findAllByAngler(Angler angler);

    Optional<Fish> findByIdAndAngler(int id, Angler angler);

}
