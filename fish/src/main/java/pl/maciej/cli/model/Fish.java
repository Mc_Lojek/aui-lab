package pl.maciej.cli.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString(callSuper = true)
@EqualsAndHashCode()
@Entity
@Table(name = "fishes") //fish has the same form for single and multiple items so I decided to name it fishes
public class Fish implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String species;
    private Float weight;
    @ManyToOne
    @JoinColumn(name = "angler")
    private Angler angler;

}
