package pl.maciej.cli.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString(callSuper = true)
@EqualsAndHashCode()
@Entity
@Table(name = "anglers")
public class Angler implements Serializable {

    @Id
    private int id;

    @ToString.Exclude
    @OneToMany(mappedBy = "angler", cascade = CascadeType.REMOVE)
    private List<Fish> fishList;

}
