package pl.maciej.cli.utils;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.model.Fish;
import pl.maciej.cli.service.AnglerService;
import pl.maciej.cli.service.FishService;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
public class Initializer {

    private final FishService fishService;
    private final AnglerService anglerService;

    @PostConstruct
    public synchronized void init() {

        if (anglerService.find(0).isEmpty()) {
            Angler a1 = Angler.builder()
                    .id(1)
                    .build();

            Angler a2 = Angler.builder()
                    .id(2)
                    .build();

            Angler a3 = Angler.builder()
                    .id(2)
                    .build();

            anglerService.create(a1);
            anglerService.create(a2);
            anglerService.create(a3);

            Fish f1 = Fish.builder().species("Carp").weight(12.55f).angler(a1).build();
            Fish f2 = Fish.builder().species("Grass Carp").weight(34.34f).angler(a1).build();
            Fish f3 = Fish.builder().species("Bream").weight(2.69f).angler(a2).build();
            Fish f4 = Fish.builder().species("Pike").weight(1.99f).angler(a3).build();

            fishService.create(f1);
            fishService.create(f2);
            fishService.create(f3);
            fishService.create(f4);
        }
    }

}
