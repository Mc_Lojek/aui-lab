package pl.maciej.cli.dto.fish;

import lombok.*;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class GetFishesResponse {

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @ToString
    @EqualsAndHashCode
    public static class Fish {
        private int id;
        private String species;
    }

    @Singular
    private List<Fish> fishes;

    public static Function<Collection<pl.maciej.cli.model.Fish>, GetFishesResponse> entityToDtoMapper() {
        return fishes -> {
            GetFishesResponseBuilder response = GetFishesResponse.builder();
            fishes.stream()
                    .map(fish -> Fish.builder()
                            .id(fish.getId())
                            .species(fish.getSpecies())
                            .build())
                    .forEach(response::fish);
            return response.build();
        };
    }
}
