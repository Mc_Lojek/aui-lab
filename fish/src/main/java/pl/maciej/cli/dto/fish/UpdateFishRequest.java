package pl.maciej.cli.dto.fish;

import lombok.*;
import pl.maciej.cli.model.Fish;

import java.util.function.BiFunction;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class UpdateFishRequest {

    private String species;
    private Float weight;

    public static BiFunction<Fish, UpdateFishRequest, Fish> dtoToEntityUpdater() {
        return (fish, request) -> {
            fish.setSpecies(request.getSpecies());
            fish.setWeight(request.getWeight());
            return fish;
        };
    }

}
