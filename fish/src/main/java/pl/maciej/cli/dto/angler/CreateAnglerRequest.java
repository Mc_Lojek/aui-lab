package pl.maciej.cli.dto.angler;

import lombok.*;
import pl.maciej.cli.model.Angler;

import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class CreateAnglerRequest {

    private int id;

    public static Function<CreateAnglerRequest, Angler> dtoToEntityMapper() {
        return request -> Angler.builder()
                .id(request.getId())
                .build();
    }

}
