package pl.maciej.cli.dto.fish;

import lombok.*;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.model.Fish;

import java.util.function.Function;
import java.util.function.Supplier;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class CreateFishRequest {

    private String species;
    private Float weight;
    private int angler;

    public static Function<CreateFishRequest, Fish> dtoToEntityMapper(
            Supplier<Angler> anglerSupplier) {
        return request -> Fish.builder()
                .species(request.getSpecies())
                .weight(request.getWeight())
                .angler(anglerSupplier.get())
                .build();
    }

}
