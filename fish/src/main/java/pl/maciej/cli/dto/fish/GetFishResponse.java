package pl.maciej.cli.dto.fish;

import lombok.*;
import pl.maciej.cli.model.Fish;

import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public class GetFishResponse {

    private int id;
    private String species;
    private Float weight;
    private int angler;

    public static Function<Fish, GetFishResponse> entityToDtoMapper() {
        return fish -> GetFishResponse.builder()
                .id(fish.getId())
                .species(fish.getSpecies())
                .weight(fish.getWeight())
                .angler(fish.getAngler().getId())
                .build();
    }

}
