package pl.maciej.cli.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.maciej.cli.dto.angler.CreateAnglerRequest;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.service.AnglerService;

import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("api/anglers")
public class AnglerController {

    private final AnglerService anglerService;

    @PostMapping
    public ResponseEntity<Void> createAngler(@RequestBody CreateAnglerRequest request, UriComponentsBuilder builder) {
        Angler angler = CreateAnglerRequest
                .dtoToEntityMapper()
                .apply(request);
        angler = anglerService.create(angler);
        return ResponseEntity.created(builder.pathSegment("api", "characters", "{id}")
                .buildAndExpand(angler.getId()).toUri()).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteAngler(@PathVariable("id") int id) {
        Optional<Angler> angler = anglerService.find(id);
        if (angler.isPresent()) {
            anglerService.delete(angler.get().getId());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
