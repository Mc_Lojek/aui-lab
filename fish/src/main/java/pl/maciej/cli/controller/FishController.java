package pl.maciej.cli.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.maciej.cli.dto.fish.CreateFishRequest;
import pl.maciej.cli.dto.fish.GetFishResponse;
import pl.maciej.cli.dto.fish.GetFishesResponse;
import pl.maciej.cli.dto.fish.UpdateFishRequest;
import pl.maciej.cli.model.Fish;
import pl.maciej.cli.service.AnglerService;
import pl.maciej.cli.service.FishService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@AllArgsConstructor
@RestController
@RequestMapping("api/fishes")
public class FishController {

    private final FishService fishService;
    private final AnglerService anglerService;

    @GetMapping
    public ResponseEntity<GetFishesResponse> getFishes() {

        System.out.println("Wchodzi tu");

        List<Fish> all = fishService.findAll();
        Function<Collection<Fish>, GetFishesResponse> mapper = GetFishesResponse.entityToDtoMapper();
        GetFishesResponse response = mapper.apply(all);
        return ResponseEntity.ok(response);
    }

    @GetMapping("{id}")
    public ResponseEntity<GetFishResponse> getFish(@PathVariable("id") int id) {
        return fishService.find(id)
                .map(value -> ResponseEntity.ok(GetFishResponse.entityToDtoMapper().apply(value)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Void> createFish(@RequestBody CreateFishRequest request, UriComponentsBuilder builder) {
        Fish fish = CreateFishRequest
                .dtoToEntityMapper(() -> anglerService.find(request.getAngler()).orElseThrow())
                .apply(request);
        fish = fishService.create(fish);
        return ResponseEntity.created(builder.pathSegment("api", "characters", "{id}")
                .buildAndExpand(fish.getId()).toUri()).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteFish(@PathVariable("id") int id) {
        Optional<Fish> fish = fishService.find(id);
        if (fish.isPresent()) {
            fishService.delete(fish.get().getId());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> updateFish(@RequestBody UpdateFishRequest request, @PathVariable("id") int id) {
        Optional<Fish> fish = fishService.find(id);
        if (fish.isPresent()) {
            UpdateFishRequest.dtoToEntityUpdater().apply(fish.get(), request);
            fishService.update(fish.get());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
