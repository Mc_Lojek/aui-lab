package pl.maciej.cli.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.maciej.cli.dto.fish.CreateFishRequest;
import pl.maciej.cli.dto.fish.GetFishResponse;
import pl.maciej.cli.dto.fish.GetFishesResponse;
import pl.maciej.cli.dto.fish.UpdateFishRequest;
import pl.maciej.cli.model.Angler;
import pl.maciej.cli.model.Fish;
import pl.maciej.cli.service.AnglerService;
import pl.maciej.cli.service.FishService;

import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("api/anglers/{id}/fishes")
public class AnglerFishController {

    private final FishService fishService;
    private final AnglerService anglerService;

    @GetMapping
    public ResponseEntity<GetFishesResponse> getFishes(@PathVariable("id") int id) {
        Optional<Angler> angler = anglerService.find(id);
        return angler.map(value -> ResponseEntity.ok(GetFishesResponse.entityToDtoMapper().apply(fishService.findAll(value))))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("{id_fish}")
    public ResponseEntity<GetFishResponse> getFish(@PathVariable("id") int id, @PathVariable("id_fish") int fishId) {
        return fishService.find(id, fishId)
                .map(value -> ResponseEntity.ok(GetFishResponse.entityToDtoMapper().apply(value)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Void> createCharacter(@PathVariable("id") int id,
                                                @RequestBody CreateFishRequest request,
                                                UriComponentsBuilder builder) {
        Optional<Angler> angler = anglerService.find(id);
        if (angler.isPresent()) {
            Fish fish = CreateFishRequest
                    .dtoToEntityMapper(angler::get)
                    .apply(request);
            fish = fishService.create(fish);
            return ResponseEntity.created(builder.pathSegment("api", "anglers", "{id}", "fishes", "{id_fish}")
                    .buildAndExpand(angler.get().getId(), fish.getId()).toUri()).build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("{id_fish}")
    public ResponseEntity<Void> deleteFish(@PathVariable("id") int id,
                                           @PathVariable("id_fish") int fishId) {
        Optional<Fish> fish = fishService.find(id, fishId);
        if (fish.isPresent()) {
            fishService.delete(fish.get().getId());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("{id_fish}")
    public ResponseEntity<Void> updateFish(@PathVariable("id") int id,
                                           @RequestBody UpdateFishRequest request,
                                           @PathVariable("id_fish") int fishId) {
        Optional<Fish> fish = fishService.find(id, fishId);
        if (fish.isPresent()) {
            UpdateFishRequest.dtoToEntityUpdater().apply(fish.get(), request);
            fishService.update(fish.get());
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
