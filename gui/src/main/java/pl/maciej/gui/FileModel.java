package pl.maciej.gui;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString(callSuper = true)
@EqualsAndHashCode()
public class FileModel {

    int id;
    String filename;
    String description;
    int fishId;

}

class FileModelResponse {
    public List<FileModel> files;
}