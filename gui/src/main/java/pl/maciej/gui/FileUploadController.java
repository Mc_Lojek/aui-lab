package pl.maciej.gui;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@AllArgsConstructor
@Controller
public class FileUploadController {

    private final FileService fileService;

    public static final String rootUrl = "http://localhost:9300/";
    public static final String apiUrl = "http://localhost:9500/";

    @GetMapping
    String getUploadForm(Model model) {
        model.addAttribute("url", rootUrl);
        model.addAttribute("apiUrl", apiUrl);
        model.addAttribute("sth", "File Upload Form");
        return "upload";
    }

    @GetMapping("/files")
    String getFiles(Model model) {
        model.addAttribute("url", rootUrl);
        model.addAttribute("apiUrl", apiUrl);
        List<FileModel> files = fileService.getAll();

        model.addAttribute("files", files);

        return "files";
    }

}
