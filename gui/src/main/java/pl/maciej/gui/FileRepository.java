package pl.maciej.gui;


import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Repository
public class FileRepository {

    public static final String url = "http://lb2/api/files";

    public FileModelResponse getAll() {
        RestTemplate restTemplate = new RestTemplate();

        FileModelResponse response = restTemplate.getForObject(url, FileModelResponse.class);

        return response;
    }

}
