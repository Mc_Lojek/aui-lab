package pl.maciej.gui;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class FileService {

    private final FileRepository fileRepository;


    public List<FileModel> getAll() {
        return fileRepository.getAll().files;
    }

}
